<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about', function(Blueprint $table){
            $table->increments('aid');
            $table->longText('overview')->nullable();
            $table->longText('facilities')->nullable();
            $table->longText('campus')->nullable();
            $table->longText('values')->nullable();
            $table->timestamps();
        });

        Schema::create('schools', function(Blueprint $table){
            $table->increments('schid');
            $table->longText('creche')->nullable();
            $table->longText('nurpri')->nullable();
            $table->longText('secondary')->nullable();
            $table->timestamps();
        });

        Schema::create('academics', function(Blueprint $table){
            $table->increments('acaid');
            $table->longText('overview')->nullable();
            $table->longText('curriculum')->nullable();
            $table->timestamps();
        });

        Schema::create('academics', function(Blueprint $table){
            $table->increments('adid');
            $table->longText('overview')->nullable();
            $table->longText('process')->nullable();
            $table->timestamps();
        });

	    Schema::create('gallery', function(Blueprint $table){
		    $table->increments('gid');
		    $table->string('name');
		    $table->string('cover');
		    $table->string('caption',7000);
		    $table->timestamps();
		    $table->softDeletes();
	    });


	    Schema::create('gallery_images', function(Blueprint $table){
		    $table->increments('giid');
		    $table->integer('gid');
		    $table->string('image');
		    $table->timestamps();
		    $table->softDeletes();
	    });

	    Schema::create('users', function (Blueprint $table) {
		    $table->increments('uid');
		    $table->string('name');
		    $table->string('email',191)->unique();
		    $table->string('password');
		    $table->rememberToken();
		    $table->timestamps();
	    });

	    Schema::create('password_resets', function (Blueprint $table) {
		    $table->string('email',191)->index();
		    $table->string('token');
		    $table->timestamp('created_at')->nullable();
	    });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery');
        Schema::dropIfExists('gallery_images');
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
    }
}
