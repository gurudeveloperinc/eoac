

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
          crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('./css/login.css')}}">
    <!-- font awesome css -->
    <link rel="stylesheet" href="{{url('./fontawesome-free-5.1.0-web/fontawesome-free-5.1.0-web/css/all.css')}}">
    <title>EOAC ADMIN</title>
    <meta name="description" content="EOAC International Schools is a modern Nursery, Primary and Secondary School located in Benin, Edo State, Nigeria that is dedicated to nurturing the next generation of problem solvers.">
    <meta name="keywords" content="schools in benin, creche, nursery, primary, secondary, secondary schools in benin, eoac, eoacis, eoac international school, eoac benin, primary schools in benin, secondary schools in benin, day schools in benin, boarding schools in benin">

</head>

<body>
<div class="container-fluid loginpage">
    <div class="row">
        <div class="col-sm-3 left-items">
            <!-- Slider -->
            <!-- End of slider -->
        </div>
        <div class="col-md-9 right-items">
            <div class= "form-content">
                <h3>Welcome back!</h3>
                <p>Please enter your details below</p>

                @if ($errors->has('email'))
                    <span class="error" role="alert">
                            <strong style="color: white">{{ $errors->first('email') }}</strong>
                        </span>
                @endif

                @if ($errors->has('password'))
                    <span class="error" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif

                <div class="form-content2">

                    <form method="post" action="{{url('login')}}">
                        @csrf
                        <input class="inputbg" type="text" id="email" name="email" placeholder="Your Email">


                        <input class="inputbg" type="password" id="password" name="password" placeholder="Your Password">


                        <input type="submit" value="SIGN IN">
                    </form>
                    <a href="{{ route('password.request') }}">Forgot password?</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>

{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Login') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<div class="form-check">--}}
                                    {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                                    {{--<label class="form-check-label" for="remember">--}}
                                        {{--{{ __('Remember Me') }}--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-8 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Login') }}--}}
                                {{--</button>--}}

                                {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                    {{--{{ __('Forgot Your Password?') }}--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endsection--}}
