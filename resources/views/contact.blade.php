@extends('layouts.app')

@section('content')

    @include('navs.smallTop')
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>Contact</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>Contact Information</h3>
            </div>
            <div class="col-12 contact-cover">
                <div class="googlemap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.445929902909!2d5.720611914769729!3d6.336237695414616!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1040d56b00d1ec11%3A0xf464d8329b46c2bf!2sEOAC+INTERNATIONAL+SCHOOLS!5e0!3m2!1sen!2sng!4v1533824486929" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="contact-head">
                    <h3>Need Help?</h3>
                    <p>Call us - 08150890770, 08150890772, 08150890773, 08078031303</p>
                    <p>Locate Us - KM 8, Benin/Agbor Express Way, Ikhueniro, Benin City, Edo State, Nigeria</p>
                    <p>Chat with us - Use the live chat at the bottom right of your screen.</p>
                    <h3>Contact us</h3>
                </div>
                <div class="contact-form">
                    <form>
                        <label for="fname">Your name *</label>
                        <input id="fname" class="form-control" name="firstname" placeholder="" type="text">
                        <label for="email">Your email address *</label>
                        <input id="email" class="form-control" name="email" placeholder="example@email.com" type="text">
                        <label for="subject">Your message *</label>
                        <textarea id="subject" name="subject" class="form-control" placeholder="" rows="8" cols="50"></textarea>
                        <div class="submit-btn">
                            <button class="btn btn-primary">Submit</button>
                            <p>Please Fill out all fields before submitting</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection