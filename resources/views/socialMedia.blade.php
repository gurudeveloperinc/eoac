@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="{{url('./css/social.css')}}">

    @include('navs.smallTop')

    <!-- What's new Section-->
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>Whats New</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>Social Media</h3>
            </div>
            {{--<div class="col-4 col-md-3">--}}
            {{--<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">--}}
            {{--<a class="nav-link active" id="v-pills-overview-tab" data-toggle="pill" href="{{url('about:blank#v-pills-overview')}}" role="tab" aria-controls="v-pills-overview" aria-selected="true">News</a>--}}
            {{--<a class="nav-link" id="v-pills-facilities-tab" data-toggle="pill" href="{{url('about:blank#v-pills-facilities')}}" role="tab" aria-controls="v-pills-facilities" aria-selected="false">Calendar</a>--}}
            {{--<a class="nav-link" id="v-pills-campus-tab" data-toggle="pill" href="{{url('about:blank#v-pills-campus')}}" role="tab" aria-controls="v-pills-campus" aria-selected="false">Social media</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="col-12 col-md-12 tab-content-section">
                <div class="tab-content" id="v-pills-tabContent" style="min-height: 400px">

                    <div class="col-md-4 col-sm-12">
                        <a href="https://twitter.com/intent/tweet?button_hashtag=eoacschools&ref_src=twsrc%5Etfw" class="twitter-hashtag-button" data-show-count="false">Tweet #eoacschools</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>

                    <div class="col-md-4 col-sm-12">

                    </div>

                    <p>FACEBOOK: @eoacis</p>
                    <p>INSTAGRAM: @eoacis</p>
                    <p>TWITTER: @eoacschools</p>
                </div>
            </div>
        </div>
    </div>

    <!-- End of What's new Section-->

@endsection