@extends('layouts.admin')

@section('content')

    <style>

        .header-background img{
            max-height: 300px;
        }
        .header-background{
            padding:50px;
        }
    </style>
    <div class="col-md-12">

        <div class="row header-background">
            <div class="col">
                <a href="{{url('admin/gallery')}}">Go Back</a>
                <p>GALLERY ID - {{$gallery->gid}}</p>
                <p>Name - {{$gallery->name}}</p>
                <p>Caption - {{$gallery->caption}}</p>
                <p>No of Images - {{count($images)}}</p>

                <form class="form-inline" enctype="multipart/form-data" method="post" action="{{url('admin/gallery/'. $gallery->gid .'/images/add')}}">

                    @csrf
                    <div class="form-group col-md-10">
                        <label style="margin-left:-35px;" class="col-sm-4 control-label">Add Images</label>
                        <div class="col-sm-6">
                            <input class="file-name input-flat" type="file" name="images[]" placeholder="Browse Files" multiple>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success">Upload</button>
                    </div>

                </form>

                <a href="{{url('admin/gallery/' . $gallery->gid . '/delete' )}}" class="btn btn-danger">Delete Gallery</a>
            </div>
            <div class="col" align="right">
                <img src="{{$gallery->cover}}" class="img img-responsive">
            </div>
        </div>


        <div class="row">

            <!-- Gallery Section-->
            <div class="container-fulid gallery">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="camera">
                                <img src="{{url('./assets/gallery.png')}}" alt="Gallery">
                                <h4>IMAGES</h4>
                            </div>

                            <div class="row images-wrapper">

                                @for($i = 0; $i < count($images); $i++)
                                <div class="col-3">
                                    <a data-fancybox="{{$gallery->name}}" data-caption="{{$gallery->caption}}" href="{{$images[$i]->image}}"><img src="{{$images[$i]->image}}" class="img img-thumbnail"></a>

                                    <a class="btn btn-danger" href="{{url('admin/gallery/image/' . $images[$i]->giid . '/delete')}}">Delete</a>
                                </div>
                                @endfor
                            </div>

                    </div>
                </div>
            </div>
            <!-- End of Gallery Section-->


        </div>
    </div>

@endsection