@extends('layouts.admin')
@section('content')

            <!-- /# row -->
            <div class="main-content">


                <div class="row">
                    <div class="col-md-6 offset-md-3  col-sm-12 col-xs-12 col-lg-6">
                        <div class="card alert">
                            <div class="card-body">
                                <div class="menu-upload-form">
                                    <form class="form-horizontal" method="post"  enctype="multipart/form-data" action="{{url('admin/gallery/add')}}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" placeholder="Type Gallery Name" name="name" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Caption</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control" rows="3" name="caption" placeholder="Short description of the gallery"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Gallery Cover</label>
                                            <div class="col-sm-8">
                                                <input class="file-name input-flat" type="file" name="image" placeholder="Browse Files">
                                            </div>
                                        </div>






                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-lg btn-primary">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /# card -->
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
            </div>
            <!-- /# main content -->


@endsection