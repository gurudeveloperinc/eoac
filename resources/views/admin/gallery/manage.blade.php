@extends('layouts.admin')

@section('content')


    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col">S/N</th>
                <th scope="col">Name</th>
                <th scope="col">Caption</th>
                <th scope="col">No of Images</th>
                <th scope="col">Date Created</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?php $count = $from ?>
            @foreach($galleries as $gallery)
            <tr>
                <th scope="row">{{$count}}</th>
                <td>{{$gallery->name}}</td>
                <td>{{str_limit($gallery->caption,100)}}</td>
                <td>{{count($gallery->Images)}}</td>
                <td>{{$gallery->created_at->toDayDateTimeString()}}</td>
                <td>
                    <a class="btn btn-success" href="{{url('admin/gallery/' . $gallery->gid)}}">View</a>
                </td>
            </tr>
                <?php $count++; ?>
            @endforeach

            </tbody>
        </table>

        {{$galleries->links()}}
    </div>

@endsection