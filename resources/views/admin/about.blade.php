@extends('layouts.admin')
@section('content')

    <!-- /# row -->
    <div class="main-content">


        <div class="row">
            <div class="col-md-12  col-sm-12 col-xs-12 col-lg-12">
                <div class="card alert">
                    <div class="card-body">
                        <div class="menu-upload-form">
                            <form class="form-horizontal" method="post"  enctype="multipart/form-data" action="{{url('admin/about')}}">
                                {{csrf_field()}}

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">OVERVIEW</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control summernote" rows="10" name="overview" placeholder="">{{$about->overview}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">FACILITIES</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control summernote" rows="10" name="facilities" placeholder="">{{$about->facilities}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">CAMPUS</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control summernote" rows="10" name="campus" placeholder="">{{$about->campus}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">VALUES</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control summernote" rows="10" name="values" placeholder="">{{$about->values}}</textarea>
                                    </div>
                                </div>




                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-lg btn-success">SAVE</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /# card -->
            </div>
            <!-- /# column -->
        </div>
        <!-- /# row -->
    </div>
    <!-- /# main content -->

@endsection
