@extends('layouts.app')

@section('content')

    @include('navs.smallTop')
    <!-- Explore Section-->
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>EXPLORE</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>TUITION AND FEES</h3>
            </div>
            <div class="col-4 col-md-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-overview-tab" data-toggle="pill" href="{{url('#v-pills-overview')}}" role="tab" aria-controls="v-pills-overview" aria-selected="true">Overview</a>
                </div>
            </div>
            <div class="col-8 col-md-9 tab-content-section">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-overview" role="tabpanel" aria-labelledby="v-pills-overview-tab">

                        <h3>SCHOOL ACCOUNT INFO</h3>
                        <p>
                            Name: EOAC International Schools Ltd<br>
                            Bank: Diamond Bank <br>
                            Account Number:  0097795440
                        </p>

                        <p>
                            Name: EOAC International Schools Ltd<br>
                            Bank: Zenith Bank <br>
                            Account Number:  1015823820
                        </p>

                        <h3>Our Policy</h3>

                        <ul>
                            <li>All fees payable into the designated school accounts above</li>
                            <li>All fees are required to be paid in full before the official resumption date of the school</li>
                            <li>All fees paid to the school account are non-refundable except expressly stated</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End of Explore Section-->

@endsection