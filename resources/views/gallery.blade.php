@extends('layouts.app')

@section('content')

    @include('navs.smallTop')
    <style>
        .gallery .image{
            float:right !important;
            min-height:200px;
        }
    </style>
    <!-- Explore Section-->
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>COMMUNITY</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>GALLERY</h3>
            </div>
            <div class="col-4 col-md-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                    @foreach($galleries as $gallery)
                    <a class="nav-link" id="gallery-{{$gallery->gid}}" data-toggle="pill" href="{{url('#' . $gallery->gid)}}" role="tab" aria-controls="{{$gallery->gid}}" aria-selected="true">{{$gallery->name}}</a>
                    @endforeach

                </div>
            </div>
            <div class="col-8 col-md-9 tab-content-section">
                <div class="tab-content" id="v-pills-tabContent">

                    @if(count($galleries) <= 0)
                        <p align="center" style="min-height: 300px;margin-top:140px;">Nothing here yet</p>
                    @endif
                    <?php $count = 1; ?>
                    @foreach($galleries as $gallery)
                    <div class="row gallery tab-pane fade
                    @if($count == 1)
                    show active
                    @endif
                    " id="{{$gallery->gid}}" role="tabpanel" aria-labelledby="gallery-{{$gallery->gid}}" >

                        @foreach($gallery->Images as $image)
                            <div class="col-3 image">
                                <a data-fancybox="{{$gallery->name}}" data-caption="{{$gallery->caption}}" href="{{$image->image}}"><img src="{{$image->image}}" class="img img-thumbnail"></a>
                            </div>
                        @endforeach

                    </div>
                        <?php $count++; ?>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

    <!-- End of Explore Section-->

@endsection