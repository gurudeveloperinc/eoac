

<style>

    .logoImage{
        width:100px;
    }

    .bg-light{
        background-color: #E74902 !important;;
    }

    nav ul li a, .navbar-brand {
        color:white !important;
    }

    .dropdown-item {
        color:black !important;
    }


</style>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{url('/')}}">

        <img src="{{url('images/logo.png')}}" class="logoImage">
        EOAC
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{url('home')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('admin/about')}}">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('admin/academics')}}">Academics</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('admin/admissions')}}">Admissions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('admin/school')}}">School</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Gallery
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{url('admin/gallery/add')}}">Add</a>
                    <a class="dropdown-item" href="{{url('admin/gallery')}}">Manage</a>
                </div>
            </li>
        </ul>

    </div>

    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="{{url('logout')}}">Logout <span class="sr-only">(current)</span></a>
        </li>

    </ul>
</nav>
