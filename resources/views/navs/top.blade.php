<!-- Top Header-->
<div class="container-fluid top-header">
    <div class="row">
        <div class="container">
            <p style="font-size: 12px;">Have questions? Call 08150890770, 08150890772</p>
            {{--<p>Email: info@eoacis.org</p>--}}
        </div>
    </div>
</div>
<!-- End of Top Header-->
<!-- Navigation -->
<div class="container-fluid header-image">
    <div class="container navbar-mobile-bg">
        <nav class="navbar navbar-expand-lg navbar-dark btco-hover-menu">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{url('./images/logo.png')}}" alt="Logo" style="height:80px;width:auto;max-width: 150px;">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{url('/')}}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Explore
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item dropdown-toggle" href="{{url('/')}}">Home</a></li>
                            <li><a class="dropdown-item dropdown-toggle" href="{{url('/about')}}">About</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="{{url('/about')}}">Facilities</a></li>
                                    <li><a class="dropdown-item" href="{{url('/about')}}">Campus</a></li>
                                    <li><a class="dropdown-item" href="{{url('/about')}}">Values</a></li>
                                </ul>
                            </li>
                            <li><a class="dropdown-item" href="{{url('/admissions')}}">Admissions</a></li>
                            <li><a class="dropdown-item" href="{{url('/admissions')}}">Academics</a></li>
                            <li><a class="dropdown-item" href="{{url('/admissions')}}">Tuition & Fees</a></li>
                            <li><a class="dropdown-item" href="{{url('/pay')}}">Online Payment</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{url('/')}}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Community
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="{{url('/gallery')}}">Gallery</a></li>
                            <li><a class="dropdown-item dropdown-toggle" href="{{url('/student-life')}}">Student Life</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="{{url('/student-life/day')}}">Day</a></li>
                                    {{--<li><a class="dropdown-item" href="{{url('/student-life/boarding')}}">Boarding</a></li>--}}
                                </ul>
                            </li>
                            <li><a class="dropdown-item" href="{{url('/careers')}}">Careers</a></li>
                            {{--<li><a class="dropdown-item" href="{{url('/faqs')}}">FAQs</a></li>--}}
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{url('/schools')}}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Schools
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="{{url('/schools')}}">Creche</a></li>
                            <li><a class="dropdown-item" href="{{url('/schools')}}">Nursery &amp; Primary</a></li>
                            <li><a class="dropdown-item" href="{{url('/schools')}}">Secondary</a></li>
                        </ul>
                    </li>

                    {{--<li class="nav-item dropdown">--}}
                        {{--<a class="nav-link dropdown-toggle" href="{{url('/')}}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                            {{--What's New--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">--}}
                            {{--<li><a class="dropdown-item" href="{{url('/news')}}">News</a></li>--}}
                            {{--<li><a class="dropdown-item" href="{{url('/calendar')}}">Calendar</a></li>--}}
                            {{--<li><a class="dropdown-item" href="{{url('/social-media')}}">Social Media</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/contact')}}">Contact</a>
                    </li>
                </ul>
                <div class="form-inline nav-right my-2 my-lg-0">
                    <a class="nav-link" href="{{url('/admissions')}}">Apply NOW</a>
                    {{--<a class="nav-link" href="{{url('/login')}}">Login</a>--}}
                </div>
            </div>
        </nav>
    </div>

    <!-- banner -->

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" style="max-height: 80vh;">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{url('./images/banner1.jpg')}}" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Modern Boarding Facilities</h5>
                    <p>Now available!</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{url('./images/banner2.jpg')}}" alt="Second slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Uncover Your Child’s</h5>
                    <p>Potentials</p>
                </div>
            </div>

            <div class="carousel-item">
                <img class="d-block w-100" src="{{url('./images/banner3.jpg')}}" alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Modern Hostel</h5>
                    <p>Facilities</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{url('./images/banner4.jpg')}}" alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Uncover Your Child’s</h5>
                    <p>Potentials</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{url('./images/banner5.jpg')}}" alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Uncover Your Child’s</h5>
                    <p>Potentials</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{url('./images/banner7.jpg')}}" alt="Fourth slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Uncover Your Child’s</h5>
                    <p>Potential</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="{{url('#carouselExampleIndicators')}}" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="{{url('#carouselExampleIndicators')}}" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!-- end banner -->

</div>
<!-- End of Navigation -->
