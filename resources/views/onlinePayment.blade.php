@extends('layouts.app')

@section('content')

    @include('navs.smallTop')

    <script src="{{url('js/vue.js')}}" ></script>
    <!-- What's new Section-->
    <div class="container main-explore" id="app">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>EXPLORE</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>ONLINE PAYMENT</h3>
            </div>
            <div class="col-12 contact-cover" align="center" style="padding: 50px;">
                {{--<div class="googlemap">--}}
                    {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3939.8817666933346!2d7.473170776755861!3d9.07453469352114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e0afc066e0ebb%3A0x32bfc18b5a474aa6!2sDiscovery+Mall!5e0!3m2!1sen!2sng!4v1532602805642" style="border:0" allowfullscreen="" width="100%" height="200" frameborder="0"></iframe>--}}
                {{--</div>--}}
                <div class="contact-head">
                    <h3>Need Help?</h3>
                    <p>Check out our Questions? section for answers to our most frequently asked questions.</p>
                    <h3>PAYMENT FORM</h3>
                </div>
                <div class="contact-form">
                    <form @submit.prevent="pay" class="col-md-6 col-md-offset-3 col-sm-12" >
                        <div class="form-group name">
                            <label for="name">Student's Name</label>
                            <input id="name" type="text" class="form-control" v-model="studentName" placeholder="Enter the student's name">
                        </div><!--//form-group-->
                        <div class="form-group name">
                            <label for="name">Amount (&#x20A6;)</label>
                            <input id="name" type="number" class="form-control" v-model="amount" placeholder="Enter amount in naira">
                        </div><!--//form-group-->
                        <div class="form-group name">
                            <label for="name">Your email (For payment receipt)</label>
                            <input id="name" type="text" class="form-control" v-model="email" placeholder="Enter your email">
                        </div><!--//form-group-->
                        <div class="form-group name">
                            <label for="name">Student's Class</label>
                            <select v-model="studentClass" class="form-control">
                                <option>Creche</option>
                                <option>Nursery 1</option>
                                <option>Nursery 2</option>
                                <option>Nursery 3</option>
                                <option>Primary 1</option>
                                <option>Primary 2</option>
                                <option>Primary 3</option>
                                <option>Primary 4</option>
                                <option>Primary 5</option>
                                <option>Primary 6</option>
                                <option>JSS 1 (Year 7)</option>
                                <option>JSS 2 (Year 8)</option>
                                <option>JSS 3 (Year 9)</option>
                                <option>SS 1 (Year 10)</option>
                                <option>SS 2 (Year 11)</option>
                                <option>SS 3 (Year 12)</option>
                            </select>

                        </div><!--//form-group-->
                        <button type="submit" class="btn btn-success" >Pay Now</button>
                        <img src="{{url('images/spinner.png')}}" style="height: 50px;" v-if="loading">
                    </form>

                </div>
            </div>
        </div>
    </div>

    <!-- End of What's new Section-->


    <script>
        var app = new Vue({
            el: '#app',
            data: {
                paymentType  : null,
                studentName  : null,
                studentClass : "JSS 1 (Year 7)",
                amount : 0,
                email: null,
                loading : false
            },
            created(){
                let raveScript = document.createElement('script');
                // raveScript.setAttribute('src', 'https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js');
                raveScript.setAttribute('src', '{{env('PAYMENT_SCRIPT')}}');
                document.head.appendChild(raveScript);

            },
            methods: {

                formatPrice(value) {
                    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                getRandomInt(max) {
                    return Math.floor(Math.random() * Math.floor(max));
                },

                pay(){

                    console.log(isFinite(this.studentName));
                    if(this.studentName === null || this.studentName === "" || this.email === "" || isFinite(this.studentName)){
                        alert("Please enter a valid name");
                        return;
                    }
                    console.log(this.studentName);
                    console.log(this.studentClass);

                    var amount = this.amount;
                    var keys = "{{env('PAYMENT_KEY')}}";

                    this.loading = true;

                    var self = this;


                    var payment = getpaidSetup({

                        PBFPubKey: keys,
                        customer_email: self.email,
                        amount: amount,
                        currency: "NGN",
                        payment_method: "both",
                        txref: "EOAC" + self.getRandomInt(999999),
                        meta: [{
                            metaname: "Name",
                            metavalue: self.studentName
                        },{
                            metaname: "Class",
                            metavalue: self.studentClass
                        },
                            {
                                metaname : "Payment For",
                                metavalue : self.paymentType
                            }],
                        onclose: function() {
                            // alert("Payment was cancelled");
                        },
                        callback: function(response) {
                            var txref = response.tx.txRef;
                            console.log("This is the response returned after a charge", response);
                            if (
                                response.tx.chargeResponseCode == "00" ||
                                response.tx.chargeResponseCode == "0"
                            ) {

                                self.reset();
                                // alert("Payment Complete");
                            } else {
                                alert("An error occurred");
                            }
                            x.close(); // use this to close the modal immediately after payment.
                            self.loading = false;
                        }

                    }).catch(e =>{ self.loading = false; });
                }
            }
        })
    </script>


@endsection