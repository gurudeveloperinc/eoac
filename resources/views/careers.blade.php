@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="{{url('./css/careers.css')}}">

    @include('navs.smallTop')

    <!-- What's new Section-->
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>COMMUNITY</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>CAREERS</h3>
            </div>
            {{--<div class="col-4 col-md-3">--}}
            {{--<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">--}}
            {{--<a class="nav-link active" id="v-pills-overview-tab" data-toggle="pill" href="{{url('about:blank#v-pills-overview')}}" role="tab" aria-controls="v-pills-overview" aria-selected="true">News</a>--}}
            {{--<a class="nav-link" id="v-pills-facilities-tab" data-toggle="pill" href="{{url('about:blank#v-pills-facilities')}}" role="tab" aria-controls="v-pills-facilities" aria-selected="false">Calendar</a>--}}
            {{--<a class="nav-link" id="v-pills-campus-tab" data-toggle="pill" href="{{url('about:blank#v-pills-campus')}}" role="tab" aria-controls="v-pills-campus" aria-selected="false">Social media</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="col-12 col-md-12 tab-content-section">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-overview" role="tabpanel" aria-labelledby="v-pills-overview-tab">


{{--                        <h3 align="center">No Vacancies at the moment</h3>--}}
                        <h5 class="m-3">The following vacancies exist at EOAC International Schools (Day and Boarding), Benin City, Edo State:  </h5>
                        <ul class="list-unstyled media-listing">
                            <li class="media">
{{--                                <img class="mr-3" src="{{url('http://via.placeholder.com/100X100')}}" alt="Generic placeholder image">--}}
                                <div class="media-body">
                                    <p>July 2nd, 2020</p>
                                    <h3 class="mt-0 mb-1"><a href="#">EARLY YEARS</a></h3>
                                    <p>
                                        Coordinator, Experienced English/Diction/Phonics Instructors /experts
                                    </p>

                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <h3 class="mt-0 mb-1"><a href="{{url('singlenews.html')}}">PRIMARY </a></h3>
                                    <p>English/Diction/Phonics and Class teachers
                                    </p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <h3 class="mt-0 mb-1"><a href="{{url('singlenews.html')}}">SECONDARY </a></h3>
                                    <p>
                                        English, French, Chemistry, Government, Music Teachers
                                    </p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <h3 class="mt-0 mb-1"><a href="#">MINIMUM REQUIREMENTS: </a></h3>
                                    <ul class="ml-3">
                                        <li>NCE, B.A.(Ed.) (English), or B.A + PGDE</li>
                                        <li>B. Ed Education</li>
                                        <li>Must display teaching dynamism of the 21st century teachers</li>
                                        <li>IGCSE Certification and Experience in teaching the British Curriculum</li>
                                        <li>Must have a minimum of 3 years cognate teaching experience and proficiency in English</li>
                                        <li>Must be passionate about teaching/working with children with high level commitment and integrity</li>
                                        <li>Must be computer savvy with the ability to use virtual classroom apps and PowerPoint presentation</li>
                                        <li>Must possess excellent interpersonal skills and be a great team player</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <h3 class="mt-0 mb-1"><a href="#">HOW TO APPLY</a></h3>
                                    <p>
                                        Interested and qualified candidates should send their CVs and cover letter to: info@eoacis.org and eoacis@gmail.com not later than 2 Weeks from the date of this publication.
                                    </p>
                                </div>
                            </li>
{{--                            <li class="media">--}}
{{--                                <img class="mr-3" src="{{url('http://via.placeholder.com/100X100')}}" alt="Generic placeholder image">--}}
{{--                                <div class="media-body">--}}
{{--                                    <p>July 17, 2017</p>--}}
{{--                                    <h3 class="mt-0 mb-1"><a href="{{url('singlenews.html')}}">List-based media object</a></h3>--}}
{{--                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in--}}
{{--                                        vulputate at, tempus viverra turpis.</p>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li class="media">--}}
{{--                                <img class="mr-3" src="{{url('http://via.placeholder.com/100X100')}}" alt="Generic placeholder image">--}}
{{--                                <div class="media-body">--}}
{{--                                    <p>July 17, 2017</p>--}}
{{--                                    <h3 class="mt-0 mb-1"><a href="{{url('singlenews.html')}}">List-based media object</a></h3>--}}
{{--                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in--}}
{{--                                        vulputate at, tempus viverra turpis.</p>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                        </ul>
                    </div>


                    {{--<div class="tab-pane fade" id="v-pills-facilities" role="tabpanel" aria-labelledby="v-pills-facilities-tab">--}}
                    {{--<h3>Facilities Content goes here</h3>--}}
                    {{--</div>--}}
                    {{--<div class="tab-pane fade" id="v-pills-campus" role="tabpanel" aria-labelledby="v-pills-campus-tab">--}}
                    {{--<h3>Campus Content goes here</h3>--}}
                    {{--</div>--}}
                    {{--<div class="tab-pane fade" id="v-pills-values" role="tabpanel" aria-labelledby="v-pills-values-tab">--}}
                    {{--<h3>Values Content goes here</h3>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>

    <!-- End of What's new Section-->

@endsection
