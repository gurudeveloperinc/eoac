@extends('layouts.app')

@section('content')

    @include('navs.smallTop')
    {{--<style>--}}
        {{--.inner-image{--}}
            {{--background: url("images/schoolsbg.jpg") center bottom no-repeat fixed !important;--}}
            {{--background-size:cover !important;--}}
        {{--}--}}
    {{--</style>--}}
    <script src="{{url('js/vue.js')}}" ></script>
    <!-- Explore Section-->
    <div class="container main-explore" id="app">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>SCHOOLS</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3></h3>
            </div>
            <div class="col-4 col-md-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-overview-tab" data-toggle="pill" href="{{url('#v-pills-overview')}}" role="tab" aria-controls="v-pills-overview" aria-selected="true">Creche</a>
                    <a class="nav-link" id="v-pills-process-tab" data-toggle="pill" href="{{url('#v-pills-process')}}" role="tab" aria-controls="v-pills-process" aria-selected="false">Nursery &amp; Primary</a>
                    <a class="nav-link" v-on:click="swap" id="v-pills-application-tab" data-toggle="pill" href="{{url('#v-pills-application')}}" role="tab" aria-controls="v-pills-application" aria-selected="false">Secondary</a>
                </div>
            </div>
            <div class="col-8 col-md-9 tab-content-section steps">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-overview" role="tabpanel" aria-labelledby="v-pills-overview-tab">
                        {!! $schools->creche !!}
                    </div>
                    <div class="tab-pane fade steps" id="v-pills-process" role="tabpanel" aria-labelledby="v-pills-process-tab">

                        {!! $schools->nurpri !!}
                    </div>
                    <div class="tab-pane fade" id="v-pills-application" role="tabpanel" aria-labelledby="v-pills-application-tab">

                        {!! $schools->secondary !!}


                        {{--<script>--}}
                            {{--var app = new Vue({--}}
                                {{--el: '#app',--}}
                                {{--data: {--}}
                                    {{--visible: false--}}
                                {{--},--}}
                                {{--methods: {--}}
                                    {{--swap(){--}}
                                        {{--console.log(this.visible); this.visible = !this.visible--}}
                                    {{--}--}}
                                {{--}--}}
                            {{--});--}}
                        {{--</script>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End of Schools Section-->


@endsection
