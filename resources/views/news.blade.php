@extends('layouts.app')

@section('content')

   <link rel="stylesheet" href="{{url('./css/news.css')}}">

   @include('navs.smallTop')

    <!-- What's new Section-->
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>Whats New</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>Latest News</h3>
            </div>
            {{--<div class="col-4 col-md-3">--}}
                {{--<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">--}}
                    {{--<a class="nav-link active" id="v-pills-overview-tab" data-toggle="pill" href="{{url('about:blank#v-pills-overview')}}" role="tab" aria-controls="v-pills-overview" aria-selected="true">News</a>--}}
                    {{--<a class="nav-link" id="v-pills-facilities-tab" data-toggle="pill" href="{{url('about:blank#v-pills-facilities')}}" role="tab" aria-controls="v-pills-facilities" aria-selected="false">Calendar</a>--}}
                    {{--<a class="nav-link" id="v-pills-campus-tab" data-toggle="pill" href="{{url('about:blank#v-pills-campus')}}" role="tab" aria-controls="v-pills-campus" aria-selected="false">Social media</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="col-12 col-md-12 tab-content-section">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-overview" role="tabpanel" aria-labelledby="v-pills-overview-tab">
                        <ul class="list-unstyled media-listing">
                            <li class="media">
                                <img class="mr-3" src="{{url('http://via.placeholder.com/100X100')}}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <p>July 17, 2017</p>
                                    <h3 class="mt-0 mb-1"><a href="{{url('singlenews.html')}}">List-based media object</a></h3>
                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
                                        vulputate at, tempus viverra turpis.</p>

                                </div>
                            </li>
                            <li class="media">
                                <img class="mr-3" src="{{url('http://via.placeholder.com/100X100')}}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <p>July 17, 2017</p>
                                    <h3 class="mt-0 mb-1"><a href="{{url('singlenews.html')}}">List-based media object</a></h3>
                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
                                        vulputate at, tempus viverra turpis.</p>
                                </div>
                            </li>
                            <li class="media">
                                <img class="mr-3" src="{{url('http://via.placeholder.com/100X100')}}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <p>July 17, 2017</p>
                                    <h3 class="mt-0 mb-1"><a href="{{url('singlenews.html')}}">List-based media object</a></h3>
                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
                                        vulputate at, tempus viverra turpis.</p>
                                </div>
                            </li>
                            <li class="media">
                                <img class="mr-3" src="{{url('http://via.placeholder.com/100X100')}}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <p>July 17, 2017</p>
                                    <h3 class="mt-0 mb-1"><a href="{{url('singlenews.html')}}">List-based media object</a></h3>
                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
                                        vulputate at, tempus viverra turpis.</p>
                                </div>
                            </li>
                            <li class="media">
                                <img class="mr-3" src="{{url('http://via.placeholder.com/100X100')}}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <p>July 17, 2017</p>
                                    <h3 class="mt-0 mb-1"><a href="{{url('singlenews.html')}}">List-based media object</a></h3>
                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
                                        vulputate at, tempus viverra turpis.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    {{--<div class="tab-pane fade" id="v-pills-facilities" role="tabpanel" aria-labelledby="v-pills-facilities-tab">--}}
                        {{--<h3>Facilities Content goes here</h3>--}}
                    {{--</div>--}}
                    {{--<div class="tab-pane fade" id="v-pills-campus" role="tabpanel" aria-labelledby="v-pills-campus-tab">--}}
                        {{--<h3>Campus Content goes here</h3>--}}
                    {{--</div>--}}
                    {{--<div class="tab-pane fade" id="v-pills-values" role="tabpanel" aria-labelledby="v-pills-values-tab">--}}
                        {{--<h3>Values Content goes here</h3>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>

    <!-- End of What's new Section-->

@endsection