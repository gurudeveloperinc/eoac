@extends('layouts.app')

@section('content')

    @include('navs.smallTop')
    <script src="{{url('js/vue.js')}}" ></script>
    <!-- Explore Section-->
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>EXPLORE</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>ADMISSIONS</h3>
            </div>
            <div class="col-4 col-md-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-overview-tab" data-toggle="pill" href="{{url('#v-pills-overview')}}" role="tab" aria-controls="v-pills-overview" aria-selected="true">Overview</a>
                    <a class="nav-link" id="v-pills-process-tab" data-toggle="pill" href="{{url('#v-pills-process')}}" role="tab" aria-controls="v-pills-process" aria-selected="false">Process</a>
                    <a class="nav-link" id="v-pills-application-tab" data-toggle="pill" href="{{url('#v-pills-application')}}" role="tab" aria-controls="v-pills-application" aria-selected="false">Application Form</a>
                    <a class="nav-link" id="v-pills-payment-tab" data-toggle="pill" href="{{url('#v-pills-payment')}}" role="tab" aria-controls="v-pills-payment" aria-selected="false">Online Payment</a>
                </div>
            </div>
            <div class="col-8 col-md-9 tab-content-section">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-overview" role="tabpanel" aria-labelledby="v-pills-overview-tab">
                        {{--<img src="{{url('./assets/banner.jpg')}}" alt="">--}}
                        {{--<h3>Welcome to EOAC International School</h3>--}}
                        {!! $admissions->overview !!}

                        {{--<p class="padding-top">Admissions are open now for entry into all classes for our Creche, Nursery and Primary schools and JS 1, JS 2, JS 3, SS 1 and SS 2 for our Secondary School.</p>--}}

                        {{--<p class="padding-top">--}}
                            {{--For more details / enquiries, interested candidates, parents and guardians can call the following numbers--}}
                            {{--08150890770, 08150890772, 08150890773, 08078031303 or email info@eoacis.org--}}
                        {{--</p>--}}

                        <div class="card-group">
                            <div class="card">
                                <div align="center" >
                                    <img style="width: 50px;height:50px;margin:40px auto;" class="card-img-top" src="{{url('assets/list.png')}}" alt="Purchase form">
                                </div>

                                <div class="card-footer">
                                    <small class="text-muted">Purchase Form</small>
                                </div>
                            </div>
                            <div class="card">
                                <img style="width: 50px;height:50px;margin:40px auto;" class="card-img-top" src="{{url('assets/vision.png')}}" alt="Our Student">
                                <div class="card-footer">
                                    <small class="text-muted">Write Entrance Exam</small>
                                </div>
                            </div>
                            <div class="card">
                                <img style="width: 50px;height:50px;margin:40px auto;" class="card-img-top" src="{{url('assets/certificate.png')}}" alt="Management">
                                <div class="card-footer">
                                    <small class="text-muted">Get Admission Letter</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade steps" id="v-pills-process" role="tabpanel" aria-labelledby="v-pills-process-tab">
                        {!! $admissions->process !!}
                    </div>
                    <div class="tab-pane fade" id="v-pills-application" role="tabpanel" aria-labelledby="v-pills-application-tab">

                        <a href="{{url('eoacapplicationform.pdf')}}" class="btn btn-primary">Download Form</a>
                    </div>
                    <div class="tab-pane fade" id="v-pills-payment" role="tabpanel" aria-labelledby="v-pills-payment-tab">
                        <div id="app">

                            <h3>Payment form here</h3>


                            <form @submit.prevent="pay" class="col-md-6 col-md-offset-3 col-sm-12" >
                                <div class="form-group name">
                                    <label for="name">Student's Name</label>
                                    <input id="name" type="text" class="form-control" v-model="studentName" placeholder="Enter the student's name">
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label for="name">Amount (&#x20A6;)</label>
                                    <input id="name" type="number" class="form-control" v-model="amount" placeholder="Enter amount in naira">
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label for="name">Your email (For payment receipt)</label>
                                    <input id="name" type="text" class="form-control" v-model="email" placeholder="Enter your email">
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label for="name">Student's Class</label>
                                    <select v-model="studentClass" class="form-control">
                                        <option>Nursery 1</option>
                                        <option>Nursery 2</option>
                                        <option>Nursery 3</option>
                                        <option>Primary 1</option>
                                        <option>Primary 2</option>
                                        <option>Primary 3</option>
                                        <option>Primary 4</option>
                                        <option>Primary 5</option>
                                        <option>Primary 6</option>
                                        <option>JSS 1 (Year 7)</option>
                                        <option>JSS 2 (Year 8)</option>
                                        <option>JSS 3 (Year 9)</option>
                                        <option>SS 1 (Year 10)</option>
                                        <option>SS 2 (Year 11)</option>
                                        <option>SS 3 (Year 12)</option>
                                    </select>

                                </div><!--//form-group-->
                                <button type="submit" class="btn btn-success">Pay Now</button>
                                <img src="{{url('assets/images/spinner.png')}}" style="height: 50px;" v-if="loading">
                            </form>
                        </div> <!-- end vue section -->

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End of Explore Section-->

    <script>
        var app = new Vue({
            el: '#app',
            data: {
                paymentType  : null,
                studentName  : null,
                studentClass : "JSS 1 (Year 7)",
                amount : 0,
                email: null,
                loading : false
            },
            created(){
                let raveScript = document.createElement('script');
                // raveScript.setAttribute('src', 'https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js');
                raveScript.setAttribute('src', '{{env('PAYMENT_SCRIPT')}}');
                document.head.appendChild(raveScript);

            },
            methods: {

                formatPrice(value) {
                    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                getRandomInt(max) {
                    return Math.floor(Math.random() * Math.floor(max));
                },

                pay(){

                    console.log(isFinite(this.studentName));
                    if(this.studentName === null || this.studentName === "" || this.email === "" || isFinite(this.studentName)){
                        alert("Please enter a valid name");
                        return;
                    }
                    console.log(this.studentName);
                    console.log(this.studentClass);

                    var amount = 5000;
                    var keys = "FLWPUBK-f90e09aeca8a955d0ee4ad0ab0ad3b57-X";

                    this.loading = true;

                    var self = this;


                    var payment = getpaidSetup({
                        // PBFPubKey: "FLWPUBK-5730a80995228b7d68b979f734ba708b-X", // live keys
                        PBFPubKey: keys,
                        customer_email: self.email,
                        amount: amount,
                        currency: "NGN",
                        payment_method: "both",
                        txref: "HC" + self.getRandomInt(999999),
                        meta: [{
                            metaname: "Name",
                            metavalue: self.studentName
                        },{
                            metaname: "Class",
                            metavalue: self.studentClass
                        },
                            {
                                metaname : "Payment For",
                                metavalue : self.paymentType
                            }],
                        onclose: function() {
                            // alert("Payment was cancelled");
                        },
                        callback: function(response) {
                            var txref = response.tx.txRef;
                            console.log("This is the response returned after a charge", response);
                            if (
                                response.tx.chargeResponseCode == "00" ||
                                response.tx.chargeResponseCode == "0"
                            ) {

                                self.reset();
                                // alert("Payment Complete");
                            } else {
                                alert("An error occurred");
                            }
                            x.close(); // use this to close the modal immediately after payment.
                            self.loading = false;
                        }

                    }).catch(e =>{ self.loading = false; });
                }
            }
        })
    </script>

@endsection
