@extends('layouts.app')

@section('content')

    @include('navs.smallTop')
    <!-- Explore Section-->
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>EXPLORE</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>ABOUT EOACIS</h3>
            </div>
            <div class="col-4 col-md-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-overview-tab" data-toggle="pill" href="{{url('#v-pills-overview')}}" role="tab" aria-controls="v-pills-overview" aria-selected="true">Overview</a>
                    <a class="nav-link" id="v-pills-facilities-tab" data-toggle="pill" href="{{url('#v-pills-facilities')}}" role="tab" aria-controls="v-pills-facilities" aria-selected="false">Facilities</a>
                    <a class="nav-link" id="v-pills-campus-tab" data-toggle="pill" href="{{url('#v-pills-campus')}}" role="tab" aria-controls="v-pills-campus" aria-selected="false">Campus</a>
                    <a class="nav-link" id="v-pills-values-tab" data-toggle="pill" href="{{url('#v-pills-values')}}" role="tab" aria-controls="v-pills-values" aria-selected="false">Values</a>
                </div>
            </div>
            <div class="col-8 col-md-9 tab-content-section">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-overview" role="tabpanel" aria-labelledby="v-pills-overview-tab">
                        <img src="{{url('images/about.jpg')}}" alt="">
                        <h3>Welcome to EOAC International Schools</h3>
                        <p>EOAC is a private institution with a public purpose. Located at KM 8, Benin/Agbor Express Way, Ikhueniro, Benin City, Edo State, Nigeria.</p>
                        <div class="card-group">
                            <div class="card">
                                <img class="card-img-top" src="{{url('images/staff.jpg')}}" alt="Our Staff">
                                <div class="card-footer">
                                    <small class="text-muted">Our Staff</small>
                                </div>
                            </div>
                            {{--<div class="card">--}}
                                {{--<img class="card-img-top" src="{{url('assets/ourstudents.jpg')}}" alt="Our Student">--}}
                                {{--<div class="card-footer">--}}
                                    {{--<small class="text-muted">Our Students</small>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="card">
                                <img class="card-img-top" src="{{url('assets/board.jpg')}}" alt="Management">
                                <div class="card-footer">
                                    <small class="text-muted">Management</small>
                                </div>
                            </div>
                        </div>

                        {!! $about->overview !!}
                    </div>
                    <div class="tab-pane fade" id="v-pills-facilities" role="tabpanel" aria-labelledby="v-pills-facilities-tab">
                        {!! $about->facilities !!}
                    </div>
                    <div class="tab-pane fade" id="v-pills-campus" role="tabpanel" aria-labelledby="v-pills-campus-tab">
                        {!! $about->campus !!}
                    </div>
                    <div class="tab-pane fade" id="v-pills-values" role="tabpanel" aria-labelledby="v-pills-values-tab">
                        {!! $about->values !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End of Explore Section-->

@endsection
