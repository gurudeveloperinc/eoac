@extends('layouts.app')

@section('content')

    @include('navs.smallTop')
    <!-- Explore Section-->
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>EXPLORE</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>STUDENT LIFE</h3>
            </div>
            <div class="col-4 col-md-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-overview-tab" data-toggle="pill" href="{{url('#v-pills-overview')}}" role="tab" aria-controls="v-pills-overview" aria-selected="true">Overview</a>
                    {{--<a class="nav-link" id="v-pills-process-tab" data-toggle="pill" href="{{url('#v-pills-process')}}" role="tab" aria-controls="v-pills-process" aria-selected="false">Day</a>--}}
                </div>
            </div>
            <div class="col-8 col-md-9 tab-content-section">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-overview" role="tabpanel" aria-labelledby="v-pills-overview-tab">
                        <img src="{{url('./assets/banner.jpg')}}" alt="">

                        <p style="padding-top: 30px;" class="justify">
                            From the classrooms filled with interesting and captivating ways of gaining knowledge to the entertaining special after school activities, student life at EOAC is perfect for any child. With beautiful scenery which calms the mind and serves as a great avenue to study, students can easily interact with each other and learn even outside the walls of the classroom.
                        </p>

                        <p>
                            Our daily schedule of activities for our students is well balanced. From arrival of students in the morning till the end of the school day in the afternoon with break intervals at appropriate times.
                        </p>

                    </div>
                    {{--<div class="tab-pane fade" id="v-pills-process" role="tabpanel" aria-labelledby="v-pills-process-tab">--}}

                    {{--</div>--}}

                </div>
            </div>
        </div>
    </div>

    <!-- End of Explore Section-->

@endsection