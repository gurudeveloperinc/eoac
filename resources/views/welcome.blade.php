@extends('layouts.app')

@section('content')

    @include('navs.top')

    <!-- Start of Tour Section-->
    <div class="container-fluid apply-section">
        <div class="row">
            <div class="col-sm-3 apply">
                <div class="apply-content">
                    <img src="{{url('./images/rubix.png')}}" alt="Logo" style="max-width: 100%; width: 150px;">
                    <p>To enroll your child or ward. Please click the button below.</p>
                    <a href="{{url('admissions')}}" class="btn btn-primary">APPLY</a>
                </div>
            </div>
            <div class="col-sm-9 apply2">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="apply-service">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="{{url('./assets/list.png')}}">
                                </div>
                                <div class="col-sm-9">
                                    <h4>Dual Curriculum</h4>
                                    <p class="justify">Our Nigeria-British Curriculum combines the Nigerian Curriculum with the full British Curriculum ensuring our students get the best opportunities.</p>
                                </div>
                            </div>
                        </div>
                        <div class="apply-service">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="{{url('./assets/teacher-pointing-blackboard.png')}}">
                                </div>
                                <div class="col-sm-9">
                                    <h4>Procedure for placement</h4>
                                    <p class="justify">Prior to being admitted, each student is assessed to recognize individual strengths and weaknesses so as to determine the best class suitable to bring out the best in them.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="apply-service">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="{{url('./assets/certificate.png')}}">
                                </div>
                                <div class="col-sm-9">
                                    <h4>Professional Teachers</h4>
                                    <p class="justify">Our assiduous teachers are provided with a conducive teaching and learning environment and use the necessary teaching tools and techniques to ensure our students are well versed in all areas. </p>
                                </div>
                            </div>
                        </div>
                        <div class="apply-service">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="{{url('./assets/team.png')}}">
                                </div>
                                <div class="col-sm-9">
                                    <h4>Core Values </h4>
                                    <p class="justify">At EOACIS we train our students not just academically, but also on morals, spirituality, possibility mentality, integrity
                                        diligence and etiquette so they can have a balanced life. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 tour">
                        {{--<p>Click here for a Virtual Tour</p>--}}
                        <img src="{{url('./assets/right-arrow-circular-button.png')}}" alt="Take Tour">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Tour Section-->
    <!-- why choose us-->
    <div class="container-fluid choose-us">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3>Why Choose EOAC International Schools</h3>
                    <p class="justify">The mission of EOACIS is to create a worldwide learning community of diverse, intellectually passionate students and teachers. Through a vibrant environment, the rigorous curriculum challenges students to be analytical and critical thinkers and problem solvers. Outside the classroom, collaborative extracurricular activities nurture lasting relationships among students and teachers. The school’s supportive environment promotes student independence, development of character, and a lifelong pursuit of knowledge.</p>
                </div>
                <div class="statement">
                    <div class="row">
                        {{--<div class="col-sm-1"></div>--}}
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="mission-holders">
                                        <h3>Our Mission</h3>
                                        <img src="{{url('./assets/mission.png')}}" alt="Mission">
                                    </div>
                                    <p class="justify">
                                        To engage in effective total nurturing (academic, moral, and spiritual) of our pupils/students in collaboration with our parents thereby empowering them to be globally relevant with high sense of responsibility, discipline and excellence
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mission-holders">
                                        <h3>Our Vision</h3>
                                        <img src="{{url('./assets/vision.png')}}" alt="Vision">
                                    </div>
                                    <p class="justify">
                                        To be a global reference point in sound holistic education by raising responsible global leaders
                                    </p>
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-sm-1"></div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of why choose us-->
    <!-- Gallery Section-->
    <div class="container-fulid gallery">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="camera">
                        <img src="{{url('./assets/gallery.png')}}" alt="Gallery">
                        <h4>EOAC Through Our Lens</h4>
                    </div>

                    <a href="{{url('gallery')}}">
                    @if(isset($gallery))
                    <div class="wrapper">
                        @if(isset($gallery->Images[0]))
                            <div class="box one" style="background-image: url('{{$gallery->Images[0]->image}}') !important;"></div>
                        @endif

                        @if(isset($gallery->Images[1]))
                            <div class="box two" style="background-image: url('{{$gallery->Images[1]->image}}') !important;"></div>
                        @endif

                        @if(isset($gallery->Images[2]))
                                <div class="box three" style="background-image: url('{{$gallery->Images[2]->image}}') !important;"></div>
                        @endif

                        @if(isset($gallery->Images[3]))
                            <div class="box four" style="background-image: url('{{$gallery->Images[3]->image}}') !important;"></div>
                        @endif

                        @if(isset($gallery->Images[4]))
                            <div class="box five" style="background-image: url('{{$gallery->Images[4]->image}}') !important;"></div>
                        @endif

                        @if(isset($gallery->Images[5]))
                                <div class="box six" style="background-image: url('{{$gallery->Images[5]->image}}') !important;"></div>
                        @endif

                        @if(isset($gallery->Images[6]))
                            <div class="box seven" style="background-image: url('{{$gallery->Images[6]->image}}') !important;"></div>
                        @endif
                    </div>
                        @else
                    <div class="wrapper">
                        <div class="box one"></div>
                        <div class="box two"></div>
                        <div class="box three"></div>
                        <div class="box four"></div>
                        <div class="box five"></div>
                        <div class="box six"></div>
                        <div class="box seven"></div>
                    </div>
                    @endif
                    </a>

                </div>
            </div>
        </div>
    </div>
    <!-- End of Gallery Section-->


@endsection
