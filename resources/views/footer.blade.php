<!-- Footer Section-->
<div class="container-fluid footer-links">
    <div class="container">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Explore</h4>
                        <ul class="footer-lists">

                            <li><a href="{{url('/')}}">Home</a> </li>
                            <li><a href="{{url('about')}}">About EOAC</a> </li>
                            <li><a href="{{url('admissions')}}">Admissions</a></li>
                            <li><a href="{{url('academics')}}">Academics</a> </li>
                            <li><a href="{{url('tuition')}}">Tuition And Fees</a></li>
                            <li><a href="{{url('online-payment')}}">Online Payment</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <h4>Community</h4>
                        <ul class="footer-lists">
                            <li><a href="{{url('gallery')}}">Gallery</a> </li>
                            <li><a href="{{url('student-life')}}">Student Life</a> </li>
                            <li><a href="{{url('careers')}}">Careers</a></li>
                            {{--<li><a href="{{url('faqs')}}">FAQs</a> </li>--}}
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <h4>Schools</h4>
                        <ul class="footer-lists">
                            <li><a href="{{url('schools')}}">Creche</a> </li>
                            <li><a href="{{url('schools')}}">Nursery &amp; Primary</a></li>
                            <li><a href="{{url('schools')}}">Secondary</a></li>
                        </ul>
                    </div>
                    {{--<div class="col-sm-4">--}}
                        {{--<h4>Whats New</h4>--}}
                        {{--<ul class="footer-lists">--}}
                            {{--<li><a href="{{url('news')}}">News</a></li>--}}
                            {{--<li><a href="{{url('calendar')}}">Calendar</a></li>--}}
                            {{--<li><a href="{{url('social-media')}}">Social Media</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
</div>
<!-- End of Footer Section-->
<!-- Copyright Section -->
<div class="container-fluid copyright">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <p>info@eoacis.org</p>
            </div>
            <div class="col-sm-4">
                <p>KM 8, Benin/Agbor Express Way, Ikhueniro, Benin City, Edo State, Nigeria</p>
            </div>
            <div class="col-sm-4">
                <p>Have Questions: 08150890770, 08150890772</p>
            </div>
            <div class="col-sm-12 right-reserved">
                <p>Copyright <?php echo date('Y') ?> EOAC International Schools.</p>
            </div>
        </div>
    </div>
</div>
