@extends('layouts.app')

@section('content')

    @include('navs.smallTop')
    <!-- Explore Section-->
    <div class="container main-explore">
        <div class="row spacing">
            <div class="col-4 col-md-3 whats-new">
                <h3>EXPLORE</h3>
            </div>
            <div class="col-8 col-md-9 overview">
                <h3>ACADEMICS</h3>
            </div>
            <div class="col-4 col-md-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-overview-tab" data-toggle="pill" href="{{url('#v-pills-overview')}}" role="tab" aria-controls="v-pills-overview" aria-selected="true">Overview</a>
                    <a class="nav-link" id="v-pills-process-tab" data-toggle="pill" href="{{url('#v-pills-process')}}" role="tab" aria-controls="v-pills-process" aria-selected="false">Curriculum</a>
                    {{--<a class="nav-link" id="v-pills-application-tab" data-toggle="pill" href="{{url('#v-pills-application')}}" role="tab" aria-controls="v-pills-application" aria-selected="false">Application Form</a>--}}
                    {{--<a class="nav-link" id="v-pills-payment-tab" data-toggle="pill" href="{{url('#v-pills-payment')}}" role="tab" aria-controls="v-pills-payment" aria-selected="false">Online Payment</a>--}}
                </div>
            </div>
            <div class="col-8 col-md-9 tab-content-section">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-overview" role="tabpanel" aria-labelledby="v-pills-overview-tab">


                        {!! $academics->overview !!}
                    </div>
                    <div class="tab-pane fade" id="v-pills-process" role="tabpanel" aria-labelledby="v-pills-process-tab">
                        {!! $academics->curriculum !!}
                    </div>
                    {{--<div class="tab-pane fade" id="v-pills-application" role="tabpanel" aria-labelledby="v-pills-application-tab">--}}
                        {{--<h3>application Content goes here</h3>--}}
                    {{--</div>--}}
                    {{--<div class="tab-pane fade" id="v-pills-payment" role="tabpanel" aria-labelledby="v-pills-payment-tab">--}}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>

    <!-- End of Explore Section-->

@endsection
