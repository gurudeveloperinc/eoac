<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'PublicController@welcome');
Route::get('about','PublicController@about');
Route::get('admissions','PublicController@admissions');
Route::get('pay','PublicController@pay');
Route::get('academics','PublicController@academics');
Route::get('tuition','PublicController@tuition');
Route::get('student-life','PublicController@studentLife');
Route::get('careers','PublicController@careers');
Route::get('faqs','PublicController@faqs');
Route::get('news','PublicController@news');
Route::get('calendar','PublicController@calendar');
Route::get('social-media','PublicController@socialMedia');
Route::get('contact','PublicController@contact');
Route::get('student-life/day','PublicController@studentLife');
Route::get('schools','PublicController@schools');


Route::get('curriculum','PublicController@curriculum');
Route::get('online-payment','PublicController@onlinePayment');
Route::get('application-form','PublicController@applicationForm');
Route::get('boarding-program','PublicController@boardingProgram');
Route::get('day-program','PublicController@dayProgram');
Route::get('vacancies','PublicController@vacancies');
Route::get('work-life','PublicController@workLife');
Route::get('gallery','PublicController@gallery');

//backend


Route::get('admin/about','HomeController@about');
Route::post('admin/about','HomeController@postAbout');

Route::get('admin/academics','HomeController@academics');
Route::post('admin/academics','HomeController@postAcademics');

Route::get('admin/school','HomeController@school');
Route::post('admin/school','HomeController@postSchool');

Route::get('admin/admissions','HomeController@admissions');
Route::post('admin/admissions','HomeController@postAdmissions');



Route::get('logout','HomeController@logout');
Route::get('admin/gallery','HomeController@gallery');
Route::get('admin/gallery/add','HomeController@addGallery');
Route::get('admin/gallery/{gid}/delete','HomeController@deleteGallery');
Route::post('admin/gallery/add','HomeController@postAddGallery');



Route::get('admin/gallery/{gid}','HomeController@galleryDetails');

Route::get('admin/gallery/image/{giid}/delete',   'HomeController@deleteImage');
Route::get('admin/gallery/{gid}/images/add','HomeController@addImages');
Route::post('admin/gallery/{gid}/images/add','HomeController@postAddImages');
