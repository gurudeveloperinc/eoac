<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class about extends Model
{
    protected $primaryKey = 'aid';
    protected $table = 'about';
}
