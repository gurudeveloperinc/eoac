<?php

namespace App\Http\Controllers;

use App\about;
use App\academic;
use App\admission;
use App\gallery;
use App\galleryImage;
use App\school;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('admin.home');
    }


	public function gallery() {

		$galleries = gallery::orderBy('created_at','desc')->paginate(20);
		return view('admin.gallery.manage',[
			'galleries' => $galleries,
			'from' => collect($galleries)->get('from')
		]);
	}



	public function addGallery(  ) {
		return view('admin.gallery.add');
	}


	public function postAddGallery(Request $request) {

		if($request->hasFile('image')){
			$filename = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('uploads',Carbon::now()->timestamp . $filename);
			$imageUrl = url('uploads/' . Carbon::now()->timestamp. $filename);
		} else {
			$imageUrl = url('images/default-image.jpg');
		}


		$gallery = new Gallery();
		$gallery->caption = $request->input('caption');
		$gallery->cover = $imageUrl;
		$gallery->name = $request->input('name');
		$gallery->save();

		session()->flash('success','Gallery Added');
		return redirect()->back();

	}


	public function galleryDetails(Request $request, $gid ) {
		$gallery = gallery::findorfail($gid);
		$images =  galleryImage::where('gid',$gid)->orderBy('created_at','desc')->paginate(20);


		return view('admin.gallery.details',[
			'gallery' => $gallery,
			'images' => $images
		]);

	}


	public function addImages($gid){
		$gallery = gallery::findorfail($gid);
		return view('backend.photos.addPhoto',[
			'gallery' => $gallery
		]);
	}


	public function postAddImages(Request $request,$gid ) {

		if($request->hasFile('images')) {
			foreach ( $request->file( 'images' ) as $item ) {
				$rand           = Str::random(5) . \Carbon\Carbon::now()->timestamp;
				$inputFileName = $item->getClientOriginalName();
				$item->move( "galleryUploads", $rand . $inputFileName );

				$images      = new galleryImage();
				$images->image = url( 'galleryUploads/'.$rand.$inputFileName );
				$images->gid = $gid;
				$images->save();
			}

				$request->session()->flash('success','Images Added');
		}

		return redirect()->back();
	}


	public function deleteGallery( $gid ) {
    	$gallery = gallery::find($gid);

    	foreach($gallery->Images as $image){

		    $urlArray = explode("/", $image->image);
		    $fileName = $urlArray[3]. "/" . $urlArray[4];
		    unlink($fileName);
		    $image->delete();

	    }

    	gallery::destroy($gid);

    	session()->flash('success','Gallery Deleted.');
    	return redirect('admin/gallery');
	}

	public function deleteImage($giid) {
		$image = galleryImage::find($giid);

		$urlArray = explode("/", $image->image);
		$fileName = $urlArray[3]. "/" . $urlArray[4];
		unlink($fileName);
		$image->delete();

		galleryImage::destroy($giid);

		session()->flash('success','Image Deleted.');
		return redirect()->back();
	}

    public function academics() {
        $academics = academic::all()->last();
        if(empty($academics)){
            $academics = new academic();
            $academics->overview = '';
            $academics->curriculum = '';
        };

        return view('admin.academics',[
            'academics' => $academics
        ]);
	}

    public function postAcademics(Request $request) {
        $overview = $request->input('overview');
        $curriculum = $request->input('curriculum');

        $academics = new academic();
        $academics->overview = $overview;
        $academics->curriculum = $curriculum;
        $academics->save();
        session()->flash('success','Academics Page Updated.');
        return redirect()->back();
	}

    public function admissions() {
        $admissions = admission::all()->last();
        if(empty($admissions)){
            $admissions = new admission();
            $admissions->overview = '';
            $admissions->curriculum = '';
        };

        return view('admin.admissions',[
            'admissions' => $admissions
        ]);
	}

    public function postAdmissions(Request $request) {
        $overview = $request->input('overview');
        $process = $request->input('process');

        $academics = new admission();
        $academics->overview = $overview;
        $academics->process = $process;
        $academics->save();
        session()->flash('success','Admissions Page Updated.');
        return redirect()->back();
	}

    public function school() {
        $school = school::all()->last();
        if(empty($school)){
            $school = new school();
            $school->creche = '';
            $school->nurpri = '';
            $school->secondary = '';
        };

        return view('admin.school',[
            'school' => $school
        ]);
	}

    public function postSchool(Request $request) {
        $creche = $request->input('creche');
        $nurpri = $request->input('nurpri');
        $secondary = $request->input('secondary');

        $school = new school();
        $school->creche = $creche;
        $school->nurpri = $nurpri;
        $school->secondary = $secondary;
        $school->save();
        session()->flash('success','School Page Updated.');
        return redirect()->back();
	}

    public function about() {
        $about = about::all()->last();

        if(empty($about)){
            $about = new about();
            $about->overview = '';
            $about->facilities = '';
            $about->campus = '';
            $about->values = '';

        }
        return view('admin.about',[
            'about' => $about
        ]);
	}

    public function postAbout(Request $request) {
        $overview = $request->input('overview');
        $facilities = $request->input('facilities');
        $campus = $request->input('campus');
        $values = $request->input('values');

        $about = new about();
        $about->overview = $overview;
        $about->facilities = $facilities;
        $about->campus = $campus;
        $about->values = $values;
        $about->save();
        session()->flash('success','About page Updated.');
        return redirect()->back();
	}


	public function logout() {
		auth()->logout();
		session()->flash('success','You have been logged out.');
		return redirect('/');
	}




}
