<?php

namespace App\Http\Controllers;

use App\about;
use App\academic;
use App\admission;
use App\gallery;
use App\school;
use Illuminate\Http\Request;

class PublicController extends Controller
{

	public function welcome() {
		$gallery = gallery::all()->last();
		return view('welcome',[
			'gallery' => $gallery
		]);
	}
	public function about() {
        $about = about::all()->last();

        return view('about',[
            'about' => $about
        ]);
    }

	public function onlinePayment() {
		return view('onlinePayment');
    }

	public function admissions() {
        $admissions = admission::all()->last();
        return view('admissions',[
        'admissions' => $admissions
        ]);
    }

	public function contact() {
		return view('contact');
    }

	public function pay() {
		return view('onlinePayment');
    }

	public function academics() {
	    $academics = academic::all()->last();
		return view('academics',[
		    'academics' => $academics
        ]);
    }

	public function studentLife() {
		return view('studentLife');
    }

	public function schools() {
	    $schools = school::all()->last();
		return view('schools',[
		    'schools' => $schools
        ]);
    }
	public function careers() {
		return view('careers');
    }

	public function faqs() {
		return view('faqs');
    }

	public function news() {
		return view('news');
    }

	public function calendar() {
		return view('calendar');
    }

	public function socialMedia() {
		return view('socialMedia');
    }

	public function tuition() {
		return view('tuition');
    }

	public function gallery() {
		$galleries = gallery::all()->sortByDesc('created_at');
		return view('gallery',[
			'galleries' => $galleries
		]);
    }

}
