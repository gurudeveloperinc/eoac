<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class school extends Model
{
    protected $primaryKey = 'schid';
    protected $table = 'schools';
}
