<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admission extends Model
{
    protected $primaryKey = 'adid';
    protected $table = 'admissions';
}
