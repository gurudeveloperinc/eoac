<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class academic extends Model
{
    protected $primaryKey = 'acaid';
    protected $table = 'academics';
}
