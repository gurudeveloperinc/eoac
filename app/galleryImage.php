<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class galleryImage extends Model
{
	protected $primaryKey = 'giid';
	protected $guarded =  [];

	public function Gallery() {
		return  $this->belongsTo(gallery::class,'gid','gid');
	}

}
