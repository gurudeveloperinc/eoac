<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{
	protected $primaryKey = 'gid';
	protected  $table = 'gallery';
	protected $guarded = [ ];

	public function Images() {
		return $this->hasMany(galleryImage::class,'gid','gid');
	}

}
